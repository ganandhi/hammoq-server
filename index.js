const Hapi = require('@hapi/hapi');
const Joi = require('@hapi/joi');
const Path = require('path');
const { REPL_MODE_SLOPPY } = require('repl');
Joi.objectId = require('joi-objectid')(Joi)
const projectConfig = require('./config/config.json');
const HammoqAuthPlugin = require('./lib/auth/hammoq-auth-filter');

const { registerUser,loginUser,getUser,updateUser,changePassword } = require('./lib/routes/route-handler');


const init = async () => {

    const server = Hapi.server({
        port: 3000,
        host: 'localhost'
    });
    
    await server.register({
        plugin: require('hapi-mongodb'),
        options: {
        //   url: 'mongodb://${conn.user}:${conn.password}@${conn.host}:${conn.port}/${conn.db}',
        url:`mongodb://${projectConfig.mongodb.user}:${projectConfig.mongodb.password}@${projectConfig.mongodb.host}:${projectConfig.mongodb.port}/${projectConfig.mongodb.db}`,
          settings: {
              useUnifiedTopology: true
          },
          decorate: true
        }
    })

     await server.register(HammoqAuthPlugin);

  
    server.auth.strategy("hammoq-auth-filter","hammoq-auth-filter");

    await server.register({
        plugin:require('inert')
    })

    
    // Add a new user to the database
server.route({
    method: 'POST',
    path: '/v1/auth/register/',
    options: {
    payload: {
        // maxBytes: 1000 * 1000 * 30, //18 MB or 8MB base64 conv file
        output: 'stream',
        allow: 'multipart/form-data',
        multipart:true
      } 
    },
    handler : async (req,h) => {
      const response = await registerUser(req);
      return response;
    }
});
server.route({
  method: 'POST',
  path: '/v1/auth/login/',
   handler : async (req,h) => {
    const response = await loginUser(req);
    return response;
  }
});
server.route({
  method: 'PUT',
  path: '/v1/auth/login/{userObjId}/',
  options: {
    auth: 'hammoq-auth-filter'
},
   handler : async (req,h) => {
  
    const response = await changePassword(req);
    return response;
  
}
});



server.route({
    method: 'GET',
    path: '/v1/user/{userId}/',
    handler: async (req, h) => {
        const response = await getUser(req);
        return response;
    },
    options: {
      auth: 'hammoq-auth-filter'
  },
});
server.route({
  method: 'PUT',
  path: '/v1/user/{userObjectId}/',
  options: {
    payload: {
        // maxBytes: 1000 * 1000 * 30, //18 MB or 8MB base64 conv file
        output: 'stream',
        allow: 'multipart/form-data',
        multipart:true
      } ,
      auth: 'hammoq-auth-filter'
    },
  handler: async (req, h) => {
      const response = await updateUser(req);
      return response;
  }
 
});
server.route({
    method: 'GET',
    path: '/images/{param*}',
    handler: {
      directory: {
        path: '../pub/images',
        redirectToSlash: true
      }
    }
  });
 /*  server.route({
    method: 'GET',
    path: '/{param*}',
    handler: {
        directory: {
            path: '../public/images'
        }
    }
}); */

    
    
    await server.start();
    console.log('Server running on %s', server.info.uri);
}

init();