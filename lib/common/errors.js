module.exports = {
    TokenNotFound : {
      statusCode: 404,
      error: 'TokenNotFound',
      message: 'Requested token for the account is not found'
    },
    InvalidCredentials: {
      statusCode: 401,
      error: 'InvalidCredentials',
      message: 'Login failed. Invalid Credentials'
    },
    InvalidPassword: {
      statusCode: 401,
      error: 'InvalidPassword',
      message: 'Invalid Password'
    },
    PermissionDenied: {
      statusCode: 401,
      error: 'Permission Denied',
      message: 'Permission Denied'
    },
    PasswordMisMatch: {
      statusCode: 401,
      error: 'PasswordMisMatch',
      message: 'New Password and Confirm New Password MisMatch'
    },
    UserNotFound: {
      statusCode: 404,
      error: 'UserNotFound',
      message: 'Username not found'
    }
   
  }