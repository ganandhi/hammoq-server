'use strict';



const Scope = 'HammoqAuthPlugin';
const jwtToken = require('jsonwebtoken');
const secret = require("../../config/jwtConfig");


const Boom = require('@hapi/boom');

const implementation = function (server, options) {
 // let jwtHelper = new JWTHelper(options.config, options.dependencies)
  return {
    authenticate: async (request, h) => {
      let cookie, decodedCookie, credentials;
      try {
        let token;
        if (request.path.includes('download')) {
          token = request.query.token;
        }
        
        cookie = request.state['hammoq-access-token'] || request.headers['hammoq-access-token'] || request.payload.header['hammoq-access-token'] ||token;
        let tmpStr = "HammoqAuthFilter, authenticate, authenticate user";
        //console.log('info', { tmpStr, cookie });
        if (cookie) {
          decodedCookie = Buffer.from(cookie, 'base64').toString();
          tmpStr = "HammoqAuthFilter, decodedCookie";
          //console.log('info', { tmpStr, decodedCookie });
          try {
            credentials = jwtToken.verify(cookie,secret.secret);
          } catch (err) {
            //console.log(err);
            //console.log('info', { message: 'Token has expired', decodedCookie});
            return h.unauthenticated(Boom.unauthorized('Token Expired or Unauthorized'));
          }
          tmpStr = 'HammoqAuthFilter', 'authenticate', 'credentials';
          //console.log('info', { tmpStr, credentials });
          if (credentials) {
            
            return h.authenticated({ credentials })
          }
          else {
            return h.unauthenticated(Boom.unauthorized('Token Expired or Unauthorized'));
          }
        } else {
          return h.unauthenticated(Boom.unauthorized('Login Required'));
         // return h.authenticated({credentials: {}});
        }
      } catch (err) {
        let stack = err.stack;
        let message = err.message;
        let details = err.details
        //console.log('error', {stack, message, details});
        if (err.error && err.error === 'TokenExpired') {
          return h.unauthenticated(Boom.unauthorized('Sorry, Your token has expired'));
        }
        return h.unauthenticated(Boom.unauthorized('Invalid Login OR Token Expired'));
      }

    }
  }
};

exports.plugin = {
  pkg: require('../../package.json'),
  name: 'HammoqAuthPlugin',
  register: (server, options) => {
    server.auth.scheme('hammoq-auth-filter', implementation);
  }
}

