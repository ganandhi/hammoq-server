//const fileUploader = require("../lib/file-service-utils.js").handleFileUpload;;
const projectConfig = require("../../config/config.json")
const secret = require("../../config/jwtConfig");
const handleFileUpload = require("../utils/file-service-utils").handleFileUpload
const Sha256 = require('js-sha3');
const jwtToken = require('jsonwebtoken');
const ObjectId = require('mongodb').ObjectId;   
const Errors = require('../common/errors');
const Boom = require('@hapi/boom');

   const registerUser =  async (req)  => {
    const userInfo = req.payload
    let { password, photo,...userDetails } = userInfo;
    userDetails.hashedPassword = Sha256.sha3_256(`${password}_${projectConfig.salt}`);

    userDetails.photoFile = await handleFileUpload(userDetails.userId,photo );
    const status = await req.mongo.db.collection('users').insertOne(userDetails);
    // return 'User Registered';
    return status;
}
const loginUser = async (req) => {
    let signedToken,currentHashedPwd,jwtInfo = {},response = {}
    try {
    const loginInfo = req.payload;
    let conditions = {
        userId: loginInfo.userId
    }
    const userInfo = await req.mongo.db.collection('users').findOne(conditions);
    currentHashedPwd = Sha256.sha3_256(`${loginInfo.password}_${projectConfig.salt}`);
    
    if(currentHashedPwd === userInfo.hashedPassword) {
        jwtInfo.userId = userInfo.userId;
        jwtInfo.email = userInfo.email; 
        jwtInfo._id = userInfo._id;
        signedToken = jwtToken.sign(jwtInfo, secret.secret, { expiresIn: 60 * 15});
    
    response = {
        id: userInfo._id,
        userId: userInfo.userId,
        email: userInfo.email,
        accessToken: signedToken
      }
      return response;
    }
    else {
        return Boom.unauthorized("Invalid password");
    }

    
}
catch(err) {
   return  Boom.unauthorized("Invalid Credentials");
}
}
const changePassword = async (req) => {
    let loginInfo = req.payload;
    
    let conditions = {
        _id : ObjectId(req.params.userObjId)
    }
    const userInfo = await req.mongo.db.collection('users').findOne(conditions);
   
    let currentHashedPwd = Sha256.sha3_256(`${loginInfo.oldPassword}_${projectConfig.salt}`);
  
    
    if(currentHashedPwd === userInfo.hashedPassword) {
       if( loginInfo.newPassword === loginInfo.confirmNewPassword) {
         let newHashedPwd = Sha256.sha3_256(`${loginInfo.newPassword}_${projectConfig.salt}`);
        let newValue = {
            $set: {
                hashedPassword : newHashedPwd}
        }
        return await req.mongo.db.collection('users').findOneAndUpdate(conditions,newValue);
       }
       else {
        
         throw Boom.conflict("Password Mismatch");
       }
    }
    else {
   
        return Boom.unauthorized('invalid password');
       
    }

}

const getUser = async (req) => {

    try {

    let conditions = {
        _id : ObjectId(req.params.userId)
    }
    const userInfo = await req.mongo.db.collection('users').findOne(conditions);
    let { hashedPassword, ...userDetails } = userInfo;
    return userDetails;
}catch(err) {
    return Boom.notFound("User Not Found");
}
}
const updateUser = async (req) => {
    let newuserInfo = {};

    newUserInfo = req.payload;
        
    let { photo, ...userDetails } = newUserInfo;

    if (newUserInfo.hasOwnProperty('photo') && newUserInfo.photo._data.length > 0) {
     userDetails.photoFile = await handleFileUpload(newUserInfo.userId,newUserInfo.photo );
    }

  
    userDetails.photoFile 

    var query = {
        _id : ObjectId(req.params.userObjectId)
    };
    let newValue = {
        $set: {
            ...userDetails}
    }

    return await req.mongo.db.collection('users').findOneAndUpdate(query,newValue);

    
}
module.exports = {
    registerUser,loginUser,getUser,updateUser,changePassword
}

 